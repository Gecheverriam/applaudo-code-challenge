package com.example.gabriel.codechallengue;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;

public class TeamDetailActivity extends AppCompatActivity {

    private VideoView mTeamVideo;
    private ImageView mTeamLogo;

    private TextView mTeamName, mTeamDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_detail);

        mTeamVideo = (VideoView) findViewById(R.id.teamVideo);
        mTeamLogo = (ImageView) findViewById(R.id.teamDetailLogo);

        mTeamName = (TextView) findViewById(R.id.teamNameDetail);
        mTeamDesc = (TextView) findViewById(R.id.teamDescription);

        Intent intent = getIntent();
        TeamModel team = (TeamModel) intent.getSerializableExtra("team");

        mTeamName.setText(team.getTeamName());
        mTeamDesc.setText(team.getDescription());

        Glide.with(getApplicationContext()).load(team.getImgLogo()).placeholder(R.drawable.placeholder).into(mTeamLogo);

        videoPlayback();

    }

    private void videoPlayback() {

        mTeamVideo.setVideoPath("http://clips.vorwaerts-gmbh.de/VfE_html5.mp4");

        MediaController mediaCont = new MediaController(this);
        mediaCont.setAnchorView(mTeamVideo);
        mTeamVideo.setMediaController(mediaCont);
        mTeamVideo.requestFocus();

        mTeamVideo.start();

    }

}
