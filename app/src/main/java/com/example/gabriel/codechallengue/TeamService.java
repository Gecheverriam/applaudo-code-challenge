package com.example.gabriel.codechallengue;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface TeamService {

    @GET("/external/applaudo_homework.json")
    Call<List<TeamModel>> getTeams();

}
