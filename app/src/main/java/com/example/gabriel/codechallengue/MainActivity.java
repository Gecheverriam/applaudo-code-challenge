package com.example.gabriel.codechallengue;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;

    private TeamAdapter mTeamAdapter;

    List<TeamModel> TeamList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerViewCreate();


    }

    private void recyclerViewCreate() {

        mRecyclerView = (RecyclerView) findViewById(R.id.rvTeamList);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

        loadJSON();

    }

    private void loadJSON() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://applaudostudios.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        TeamService request = retrofit.create(TeamService.class);


        Call<List<TeamModel>> call = request.getTeams();

        call.enqueue(new Callback<List<TeamModel>>() {
            @Override
            public void onResponse(Call<List<TeamModel>> call, Response<List<TeamModel>> response) {

                List<TeamModel> teamList = response.body();

                mTeamAdapter = new TeamAdapter(teamList, new TeamAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(TeamModel item) {

                        Intent intent = new Intent(MainActivity.this, TeamDetailActivity.class);

                        intent.putExtra("team", item);

                        startActivity(intent);

                    }
                });

                mRecyclerView.setAdapter(mTeamAdapter);

            }

            @Override
            public void onFailure(Call<List<TeamModel>> call, Throwable t) {
                Log.d("Error: ",t.getMessage());
            }
        });

    }
}
