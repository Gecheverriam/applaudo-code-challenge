package com.example.gabriel.codechallengue;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestManager {

    private TeamService mTeamService;

    public static final String BASE_URL = "http://applaudostudios.com";

    public TeamService getmTeamService() {
        if(mTeamService == null) {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            mTeamService = retrofit.create(TeamService.class);

        }

        return mTeamService;

    }

}
