package com.example.gabriel.codechallengue;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.MyViewHolder> {

    private final OnItemClickListener mListener;
    private List<TeamModel> mTeamList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView mLogo;
        private TextView mName, mAddress;

        public MyViewHolder(View view) {
            super(view);

            mLogo = (ImageView) view.findViewById(R.id.logo_image);

            mName = (TextView) view.findViewById(R.id.team_name);
            mAddress = (TextView) view.findViewById(R.id.address);

        }

        public void bind(final TeamModel teamModel, final OnItemClickListener mListener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    mListener.onItemClick(teamModel);
                }
            });

        }

    }

    public TeamAdapter(List<TeamModel> teamList, OnItemClickListener listener) {

        mTeamList = teamList;

        mListener = listener;

    }

    @Override
    public  MyViewHolder onCreateViewHolder(ViewGroup parent, int ViewType) {

        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.team_list_row, null, false);

        return new MyViewHolder(row);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.bind(mTeamList.get(position),mListener);

        holder.mName.setText(mTeamList.get(position).getTeamName());
        holder.mAddress.setText(mTeamList.get(position).getAddress());
        Glide.with(holder.itemView.getContext()).load(mTeamList.get(position).getImgLogo()).placeholder(R.drawable.placeholder).into(holder.mLogo);

    }

    @Override
    public int getItemCount() {
        return mTeamList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(TeamModel item);
    }

}